# Airborne Coast Monitoring System for Sea Turtle Detection and Species Classification

<p align="center"> 
<img src="images/context.jpg" />
</p>

### Written using React.js and Semantic UI.
### Fully firebase integrated: Real-time DB, Firestore, Storage, Hosting, Auth, ML Kit



![Screenshot](images/homepage.jpg)

![Screenshot](images/start_mission.jpg)

![](images/editmission.gif)

![Screenshot](images/active_mission.jpg)

![Screenshot](images/result_mission.jpg)

![Screenshot](images/history_1.jpg)
![Screenshot](images/history_2.jpg)
![Screenshot](images/history_3.jpg)
![Screenshot](images/history_4.jpg)
![Screenshot](images/history_5.jpg)
![Screenshot](images/history_6.jpg)



